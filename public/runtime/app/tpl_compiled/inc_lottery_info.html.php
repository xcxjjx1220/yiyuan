<?php if ($this->_var['lottery_info']['luck_user_id'] > 0): ?>

	<div class="goods-record">
	<p class="owner"><?php echo $this->_var['LANG']['Home_lottery_info_Winner']; ?><a href=""><?php echo $this->_var['lottery_info']['user_name']; ?></a></p>
	<p><?php echo $this->_var['LANG']['Home_lottery_info_Participation_in_this_issue']; ?><?php echo $this->_var['lottery_info']['current_buy']; ?>Num</p>
	<p><?php echo $this->_var['LANG']['Home_lottery_info_Lucky_number']; ?><?php echo $this->_var['lottery_info']['lottery_sn']; ?></p>
	</div>
<?php else: ?>

	<div class="goods-counting">
	<div class="goods-countdown">
		<?php echo $this->_var['LANG']['Home_lottery_info_Announce_the_countdown']; ?>
		<div class="countdown">
			<span class="countdown-nums">
				<time class="w-countdown-nums" nowtime="<?php echo $this->_var['now_time']; ?>" endtime="<?php echo $this->_var['lottery_info']['lottery_time']; ?>" id="<?php echo $this->_var['lottery_info']['id']; ?>">
				<?php echo $this->_var['LANG']['Home_lottery_info_Being_announced']; ?>
			    </time>

			</span>
		</div>
	</div>
	</div>
<?php endif; ?>