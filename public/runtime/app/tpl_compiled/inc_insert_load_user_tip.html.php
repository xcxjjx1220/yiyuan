<?php if ($this->_var['user_info']): ?>
	<span class="user_name">
		<?php echo $this->_var['LANG']['Home_load_user_tip_Welcome']; ?>，<a href="<?php
echo parse_url_tag("u:index|uc_center|"."".""); 
?>" ><?php echo $this->_var['user_info']['user_name']; ?></a>
		<?php if ($this->_var['user_info']['level'] > 0): ?>
		<span title="<?php echo $this->_var['user_info']['level_name']; ?>" class="level_bg level_<?php echo $this->_var['user_info']['level']; ?>"></span>
		<?php endif; ?>
		[ <a href="<?php
echo parse_url_tag("u:index|user#loginout|"."".""); 
?>"><?php echo $this->_var['LANG']['Home_load_user_tip_log_out']; ?></a> ]
	</span>
	<?php if ($this->_var['user_info']['msg_count'] > 0): ?>
	<a href="<?php
echo parse_url_tag("u:index|uc_msg|"."".""); 
?>" class="msg_count" title="<?php echo $this->_var['LANG']['Home_load_user_tip_Total']; ?><?php echo $this->_var['user_info']['msg_count']; ?><?php echo $this->_var['LANG']['Home_load_user_tip_New_posts']; ?>"><span><i class="iconfont">&#xe62c;</i> <?php echo $this->_var['LANG']['Home_load_user_tip_news']; ?> <em><?php echo $this->_var['user_info']['msg_count']; ?></em></span></a>
<?php else: ?>
	<?php endif; ?>
	<script type="text/javascript">
			init_drop_user();
			<?php if ($this->_var['signin_result']): ?>
			show_signin_message(<?php echo $this->_var['signin_result']; ?>);
			<?php endif; ?>
	</script>
	<?php else: ?>
	<span class="login_tip"><?php echo $this->_var['LANG']['Home_load_user_tip_Please_first']; ?> [<a href="<?php
echo parse_url_tag("u:index|user#login|"."".""); 
?>" title="<?php echo $this->_var['LANG']['Home_load_user_tip_login']; ?>" id="pop_login"><?php echo $this->_var['LANG']['Home_load_user_tip_login']; ?></a>]<?php if ($this->_var['wx_login']): ?> / [<a href="javascript:void(0);" rel="<?php
echo parse_url_tag("u:index|user#wx_login|"."".""); 
?>" title="<?php echo $this->_var['LANG']['Home_load_user_tip_WeChat_login']; ?>" id="wx_login"><?php echo $this->_var['LANG']['Home_load_user_tip_WeChat_login']; ?></a>]<?php endif; ?> <?php echo $this->_var['LANG']['Home_load_user_tip_or']; ?> [<a href="<?php
echo parse_url_tag("u:index|user#register|"."".""); 
?>" title="<?php echo $this->_var['LANG']['Home_load_user_tip_register']; ?>"><?php echo $this->_var['LANG']['Home_load_user_tip_register']; ?></a>]</span>
<?php endif; ?>
<span id="user_drop">
	<a href="<?php
echo parse_url_tag("u:index|uc_duobao|"."".""); 
?>"><?php echo $this->_var['LANG']['Home_load_user_tip_My_treasure']; ?></a>
	<i class="iconfont">&#xe610;</i>
</span>
<div id="user_drop_box">
	<dl>
		<dd><a href="<?php
echo parse_url_tag("u:index|uc_duobao|"."".""); 
?>"><?php echo $this->_var['LANG']['Home_load_user_tip_Treasure_Record']; ?></a></dd>
		<dd class="group"><a href="<?php
echo parse_url_tag("u:index|uc_luck|"."".""); 
?>"><?php echo $this->_var['LANG']['Home_load_user_tip_Lucky_record']; ?></a></dd>
		<dd><a href="<?php
echo parse_url_tag("u:index|uc_share|"."".""); 
?>"><?php echo $this->_var['LANG']['Home_load_user_tip_My_order']; ?></a></dd>
		<dd><a href="<?php
echo parse_url_tag("u:index|uc_money|"."".""); 
?>"><?php echo $this->_var['LANG']['Home_load_user_tip_Account_recharge']; ?></a></dd>
	</dl>
</div>
<a href="<?php
echo parse_url_tag("u:index|uc_voucher|"."".""); 
?>"><?php echo $this->_var['LANG']['Home_load_user_tip_My_red_envelope']; ?></a>