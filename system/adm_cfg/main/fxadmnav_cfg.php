<?php 
return array(
//fx 营销菜单位于订单菜单后面
	"marketing"	=>	array(
			"name"	=>	L("fxadmnav_cfg_Marketing_manage"),
			"key"	=>	"marketing",
			"groups"	=>	array(
					"fx"	=>	array(
							"name"	=>	L("fxadmnav_cfg_Distribution_manage"),
							"key"	=>	"fx",
							"nodes"	=>	array(
								array("name"=>L("fxadmnav_cfg_Distribution_settings"),"module"=>"FxSalary","action"=>"index"),
							    array("name"=>L("fxadmnav_cfg_Distribution_member"),"module"=>"FxUser","action"=>"index"),
							),
					),
			),
	),
);
?>