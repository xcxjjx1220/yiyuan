<?php 
return array( 
	"index"	=>	array(
		"name"	=>	L("admnav_cfg_Home"),
		"key"	=>	"index", 
		"groups"	=>	array( 
			"index"	=>	array(
				"name"	=>	L("admnav_cfg_Home"),
				"key"	=>	"index", 
				"nodes"	=>	array( 
					array("name"=>L("admnav_cfg_Home"),"module"=>"Index","action"=>"main"),
				),
			),
			"balance"	=>	array(
				"name"	=>	L("admnav_cfg_Report"),
				"key"	=>	"balance",
				"nodes"	=>	array(
						array("name"=>L("admnav_cfg_Report_statistics"),"module"=>"Balance","action"=>"index"),
				        array("name"=>L("admnav_cfg_Income_statistics"),"module"=>"IncomeBalance","action"=>"index")
				),
			),
		),
	),
	"deal"	=>	array(
		"name"	=>	L("admnav_cfg_Project_management"),
		"key"	=>	"deal", 
		"groups"	=>	array( 			
			"deal"	=>	array(
				"name"	=>	L("admnav_cfg_Commodity_maintenance"),
				"key"	=>	"deal", 
				"nodes"	=>	array( 
					array("name"=>L("admnav_cfg_Commodity_maintenance"),"module"=>"Deal","action"=>"index"),
					array("name"=>L("admnav_cfg_Classification_management"),"module"=>"DealCate","action"=>"index"),
					array("name"=>L("admnav_cfg_Brand_management"),"module"=>"Brand","action"=>"index"),
				),
			),			
			"duobao"	=>	array(
					"name"	=>	L("admnav_cfg_Treasure_hunt_activity"),
					"key"	=>	"duobao",
					"nodes"	=>	array(
							array("name"=>L("admnav_cfg_Treasure_Hunt_Project"),"module"=>"Duobao","action"=>"index"),
							array("name"=>L("admnav_cfg_Treasure_hunt_activity"),"module"=>"DuobaoItem","action"=>"index"),
							//array("name"=>"往期夺宝","module"=>"DuobaoItemHistory","action"=>"index"),
					),
			),
		),
	),
	"order"	=>	array(
			"name"	=>	L("admnav_cfg_Order_management"),
			"key"	=>	"order",
			"groups"	=>	array(
					"order"	=>	array(
							"name"	=>	L("admnav_cfg_Order_management"),
							"key"	=>	"order",
							"nodes"	=>	array(
									array("name"=>L("admnav_cfg_Treasure_Order"),"module"=>"DuobaoOrder","action"=>"index"),
									//array("name"=>"往期夺宝订单","module"=>"DuobaoOrder","action"=>"trash"),
									array("name"=>L("admnav_cfg_Winning_order"),"module"=>"DealOrder","action"=>"index"),
									//array("name"=>"往期中奖订单","module"=>"DealOrder","action"=>"trash"),
									array("name"=>L("admnav_cfg_Top_up_order"),"module"=>"InchargeOrder","action"=>"index"),
									//array("name"=>"往期充值订单","module"=>"InchargeOrder","action"=>"trash"),
									array("name"=>L("admnav_cfg_List_of_receipts"),"module"=>"PaymentNotice","action"=>"index"),
									//array("name"=>"订单来路统计","module"=>"DealOrder","action"=>"referer"),
							),
					),					
					"orderinterface"	=>	array(
							"name"	=>	L("admnav_cfg_Transaction_related_business"),
							"key"	=>	"orderinterface",
							"nodes"	=>	array(
									array("name"=>L("admnav_cfg_Payment_settings"),"module"=>"Payment","action"=>"index"),
									array("name"=>L("admnav_cfg_Red_envelope_management"),"module"=>"EcvType","action"=>"index"),
							),
					),
// 					"delivery"	=>	array(
// 							"name"	=>	"配送方式",
// 							"key"	=>	"delivery",
// 							"nodes"	=>	array(
// 									array("name"=>"配送地区列表","module"=>"DeliveryRegion","action"=>"index"),									
// 							),
// 					),
					
			),
	),		
	
	"user"	=>	array(
			"name"	=>	L("admnav_cfg_Member_Management"),
			"key"	=>	"user",
			"groups"	=>	array(
					"user"	=>	array(
							"name"	=>	L("admnav_cfg_Member_Management"),
							"key"	=>	"user",
							"nodes"	=>	array(
									array("name"=>L("admnav_cfg_member_list"),"module"=>"User","action"=>"index"),
									//array("name"=>"会员提现","module"=>"User","action"=>"withdrawal_index"),
							),
					),								
					"notice"	=>	array(
							"name"	=>	L("admnav_cfg_Station_news"),
							"key"	=>	"notice",
							"nodes"	=>	array(
									array("name"=>L("admnav_cfg_Group_message"),"module"=>"MsgSystem","action"=>"index"),
									array("name"=>L("admnav_cfg_Message_list"),"module"=>"MsgBox","action"=>"index"),
							),
					),					
					"msgadmin"	=>	array(
							"name"	=>	L("admnav_cfg_Member_post"),
							"key"	=>	"msgadmin",
							"nodes"	=>	array(
									array("name"=>L("admnav_cfg_List_management"),"module"=>"Share","action"=>"index"),
							),
					),
			),
	),	
	"promote"	=>	array(
		"name"	=>	L("admnav_cfg_Scheduled_Tasks"),
		"key"	=>	"promote", 
		"groups"	=>	array( 
			"msg"	=>	array(
				"name"	=>	L("admnav_cfg_Message_template_manage"),
				"key"	=>	"msg", 
				"nodes"	=>	array( 
					array("name"=>L("admnav_cfg_Message_template_manage"),"module"=>"MsgTemplate","action"=>"index"),
				),
			),
// 			"mail"	=>	array(
// 				"name"	=>	"邮件管理", 
// 				"key"	=>	"mail", 
// 				"nodes"	=>	array( 
// 					array("name"=>"邮件服务器列表","module"=>"MailServer","action"=>"index"),
// 					array("name"=>"邮件列表","module"=>"PromoteMsg","action"=>"mail_index"),
// 				),
// 			),
			"sms"	=>	array(
				"name"	=>	L("admnav_cfg_SMS_management"),
				"key"	=>	"sms", 
				"nodes"	=>	array( 
					array("name"=>L("admnav_cfg_SMS_interface_list"),"module"=>"Sms","action"=>"index"),
				),
			),
			"msglist"	=>	array(
				"name"	=>	L("admnav_cfg_Scheduled_Tasks"),
				"key"	=>	"msglist", 
				"nodes"	=>	array( 
					array("name"=>L("admnav_cfg_Scheduled_task_list"),"module"=>"ScheduleList","action"=>"index"),
					array("name"=>L("admnav_cfg_Third_party_lottery_results"),"module"=>"FairFetch","action"=>"index"),
				),
			),
		),
	),		
	"mobile"	=>	array(
			"name"	=>	L("admnav_cfg_mobile_platform"),
			"key"	=>	"mobile",
			"groups"	=>	array(						
				"mobile"	=>	array(
					"name"	=>	L("admnav_cfg_Mobile_platform_settings"),
					"key"	=>	"mobile", 
					"nodes"	=>	array( 
						array("name"=>L("admnav_cfg_App_configuration"),"module"=>"Conf","action"=>"mobile"),
						//array("name"=>"手机端专题位","module"=>"MZt","action"=>"index"),
						array("name"=>L("admnav_cfg_List_of_mobile_ads"),"module"=>"MAdv","action"=>"index"),
						array("name"=>L("admnav_cfg_Home_menu_list"),"module"=>"MIndex","action"=>"index"),
					),
				),					
			),
	),
	"system"	=>	array(
		"name"	=>	L("admnav_cfg_System_settings"),
		"key"	=>	"system", 
		"groups"	=>	array( 
			"sysconf"	=>	array(
				"name"	=>	L("admnav_cfg_System_settings"),
				"key"	=>	"sysconf", 
				"nodes"	=>	array( 
					array("name"=>L("admnav_cfg_System_Configuration"),"module"=>"Conf","action"=>"index"),
					array("name"=>L("admnav_cfg_Navigation_menu"),"module"=>"Nav","action"=>"index"),
					array("name"=>L("admnav_cfg_Ad_settings"),"module"=>"Adv","action"=>"index"),
					array("name"=>L("admnav_cfg_Delivery_area_list"),"module"=>"DeliveryRegion","action"=>"index"),
					array("name"=>L("admnav_cfg_Announce_and_Service_Agree"),"module"=>"Agreement","action"=>"index"),
						
				),
			),
			"article"	=>	array(
					"name"	=>	L("admnav_cfg_Site_help"),
					"key"	=>	"article",
					"nodes"	=>	array(
						array("name"=>L("admnav_cfg_Mobile_phone_help_list"),"module"=>"Article","action"=>"index"),
						array("name"=>L("admnav_cfg_Mobile_phone_help_classification"),"module"=>"ArticleCate","action"=>"index"),
						array("name"=>L("admnav_cfg_Web_side_help_list"),"module"=>"WebArticle","action"=>"index"),
						array("name"=>L("admnav_cfg_Help_classification_on_the_web"),"module"=>"WebArticleCate","action"=>"index"),
					),
			),				
			"admin"	=>	array(
				"name"	=>	L("admnav_cfg_System_administrator"),
				"key"	=>	"admin", 
				"nodes"	=>	array( 
					array("name"=>L("admnav_cfg_Role_management"),"module"=>"Role","action"=>"index"),
					array("name"=>L("admnav_cfg_Administrator_management"),"module"=>"Admin","action"=>"index"),
				),
			),
			"datebase"	=>	array(
				"name"	=>	L("admnav_cfg_database"),
				"key"	=>	"datebase", 
				"nodes"	=>	array( 
					array("name"=>L("admnav_cfg_database_backup"),"module"=>"Database","action"=>"index"),
					array("name"=>L("admnav_cfg_SQL_operation"),"module"=>"Database","action"=>"sql"),
				),
			),
			"syslog"	=>	array(
				"name"	=>	L("admnav_cfg_System_log"),
				"key"	=>	"syslog", 
				"nodes"	=>	array( 
					array("name"=>L("admnav_cfg_System_log_list"),"module"=>"Log","action"=>"index"),
					
				),
			),
			
		),
	),
);
?>