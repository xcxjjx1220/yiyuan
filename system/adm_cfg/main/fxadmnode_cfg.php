<?php 
return array( 
	    //fx 分销模块 -------start---------------------------	
	"FxStatement"	=>	array(
			"name"	=>	L("fxadmnode_cfg_Distribution_report"),
			"node"	=>	array(
					"index"	=>	array("name"=>L("fxadmnode_cfg_Report_view"),"action"=>"index"),
					"foreverdelete"	=>	array("name"=>L("fxadmnode_cfg_Delete_report"),"action"=>"foreverdelete"),
			)
	),
		
    "Fxsalary"	=>	array(
        "name"	=>	L("fxadmnode_cfg_Distribution_set"),
        "node"	=>	array(
            "index"	=>	array("name"=>L("fxadmnode_cfg_Global_distribution_commission_set"),"action"=>"index"),
            "level_index"	=>	array("name"=>L("fxadmnode_cfg_Membership_level_commission_set"),"action"=>"level_index"),
            "add_level"	=>	array("name"=>L("fxadmnode_cfg_Add_distribution_level"),"action"=>"add_level"),
            "edit_level"	=>	array("name"=>L("fxadmnode_cfg_Edit_distribution_level"),"action"=>"edit_level"),
            "deal_index"	=>	array("name"=>L("fxadmnode_cfg_Commission_set_for_distribution_products"),"action"=>"deal_index"),
            "add_deal"	=>	array("name"=>L("fxadmnode_cfg_Add_distribution_products"),"action"=>"add_deal"),
            "save"	=>	array("name"=>L("fxadmnode_cfg_Save_distribution_data"),"action"=>"save"),
            "save_1"	=>	array("name"=>L("fxadmnode_cfg_Save_global_distribution_commission_set"),"action"=>"save_1"),
            "save_2"	=>	array("name"=>L("fxadmnode_cfg_Save_membership_level_commission_set"),"action"=>"save_2"),
            "save_3"	=>	array("name"=>L("fxadmnode_cfg_Save_distribution_commodity_commission_set"),"action"=>"save_3"),
        )
    ),
    
    //fx 分销模块 -------end---------------------------
    
);
?>