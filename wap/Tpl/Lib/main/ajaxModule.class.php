<?php
// +----------------------------------------------------------------------
// | fanwebbs.com 一元技术论坛
// +----------------------------------------------------------------------
// | Copyright (c) 2016 http://www.fanwebbs.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 微柚（5773389@qq.com）
// +----------------------------------------------------------------------

class ajaxModule extends MainBaseModule
{

	public function send_sms_code()
	{
		$mobile = strim($_REQUEST['mobile']);
		$unique = intval($_REQUEST['unique']);
		$verify_code = strim($_REQUEST['verify_code']);
                $account = intval($_REQUEST['account']);
		$data = request_api("sms","send_sms_code",array("mobile"=>$mobile,"unique"=>$unique,"verify_code"=>$verify_code,"account"=>$account));
		ajax_return($data);
	}

	public function send_fxsms_code()
	{
		global_run();

		$mobile = $GLOBALS['user_info']['mobile'];
		$unique = intval($_REQUEST['unique']);
		$verify_code = strim($_REQUEST['verify_code']);
		if($mobile==""){
			$data['status']=0;
			$data['info']="Please complete the member phone number";
			ajax_return($data);
		}
		$data = request_api("sms","send_sms_code",array("mobile"=>$mobile,"unique"=>$unique,"verify_code"=>$verify_code));
		ajax_return($data);
	}

	public function close_appdown()
	{
		es_cookie::set('is_app_down',1,3600*24*7);
	}


	public function count_buy_total()
	{
		
		$ecvsn = $_REQUEST['ecvsn']?addslashes(trim($_REQUEST['ecvsn'])):'';
		$ecvpassword = $_REQUEST['ecvpassword']?addslashes(trim($_REQUEST['ecvpassword'])):'';
		$payment = intval($_REQUEST['payment']);
		$all_account_money = intval($_REQUEST['all_account_money']);

		$data = request_api("cart","count_buy_total",array("ecvsn"=>$ecvsn,"ecvpassword"=>$ecvpassword,"payment"=>$payment,"all_account_money"=>$all_account_money));

		$feeinfo['feeinfo'] = $data['feeinfo'];
		$GLOBALS['tmpl']->assign("data",$feeinfo);
		$ajaxdata['html'] = $GLOBALS['tmpl']->fetch("inc/cart_total.html");
		$ajaxdata['pay_price'] = $data['pay_price'];
		$ajaxdata['is_pick'] = $data['is_pick'];
		ajax_return($ajaxdata);
	}

	public function count_order_total()
	{
		$order_id = intval($_REQUEST['id']);
		$delivery_id =  intval($_REQUEST['delivery_id']); //配送方式
		$payment = intval($_REQUEST['payment']);
		$all_account_money = intval($_REQUEST['all_account_money']);

		$data = request_api("cart","count_order_total",array("id"=>$order_id,"delivery_id"=>$delivery_id,"payment"=>$payment,"all_account_money"=>$all_account_money));

		$feeinfo['feeinfo'] = $data['feeinfo'];
		$GLOBALS['tmpl']->assign("data",$feeinfo);
		$ajaxdata['html'] = $GLOBALS['tmpl']->fetch("inc/cart_total.html");
		$ajaxdata['pay_price'] = $data['pay_price'];
		$ajaxdata['delivery_fee_supplier'] = $data['delivery_fee_supplier'];
		$ajaxdata['delivery_info'] = $data['delivery_info'];
		$ajaxdata['is_pick'] = $data['is_pick'];
		ajax_return($ajaxdata);
	}

	public function focus(){
	    global_run();
	    $param=array();
	    $param['uid'] = intval($_REQUEST['uid']);

	    $data = request_api("uc_home","focus",$param);

	    if($data['user_login_status']!=LOGIN_STATUS_LOGINED){
	        $data['info'] = "Please login first";
	        $data['jump'] = wap_url("index","user#login");
	    }

	    ajax_return($data);
	}


	public function do_refund_coupon()
	{
		global_run();
		init_app_page();

		$param['id'] = intval($_REQUEST['id']);
		$data = request_api("biz_ordermanage","do_refund_coupon",$param);


		if ($data['biz_user_status']==0){ //用户未登录
			$return['status'] = 1000;
			ajax_return($return);
		}
		else
		{
			ajax_return($data);
		}
	}

	public function do_refuse_coupon()
	{
		global_run();
		init_app_page();

		$param['id'] = intval($_REQUEST['id']);
		$data = request_api("biz_ordermanage","do_refuse_coupon",$param);


		if ($data['biz_user_status']==0){ //用户未登录
			$return['status'] = 1000;
			ajax_return($return);
		}
		else
		{
			ajax_return($data);
		}
	}


	public function do_refund_item()
	{
		global_run();
		init_app_page();

		$param['id'] = intval($_REQUEST['id']);
		$data = request_api("biz_ordermanage","do_refund_item",$param);


		if ($data['biz_user_status']==0){ //用户未登录
			$return['status'] = 1000;
			ajax_return($return);
		}
		else
		{
			ajax_return($data);
		}
	}

	public function do_refuse_item()
	{
		global_run();
		init_app_page();

		$param['id'] = intval($_REQUEST['id']);
		$data = request_api("biz_ordermanage","do_refuse_item",$param);


		if ($data['biz_user_status']==0){ //用户未登录
			$return['status'] = 1000;
			ajax_return($return);
		}
		else
		{
			ajax_return($data);
		}
	}

	public function do_verify_delivery()
	{
		global_run();
		init_app_page();

		$param['id'] = intval($_REQUEST['id']);
		$data = request_api("biz_ordermanage","do_verify_delivery",$param);


		if ($data['biz_user_status']==0){ //用户未登录
			$return['status'] = 1000;
			ajax_return($return);
		}
		else
		{
			ajax_return($data);
		}
	}

	public function add_cart(){
            $data_id = intval($_REQUEST['data_id']);
            $buy_num = intval($_REQUEST['buy_num']);
           
            $data = request_api("cart","addcart",array("data_id"=>$data_id,"buy_num"=>$buy_num));
         
            $ajax_data['status'] = $data['status'];
            $ajax_data['info'] = $data['info'];
            $ajax_data['cart_item_num'] = $data['cart_item_num'];
            $ajax_data['jump'] = wap_url("index","user#login");
            ajax_return($ajax_data);
	}
        
        public function get_cart(){
            $data = request_api("cart","getcart");
            ajax_return($result);
        }
        
        public function del_cart(){
            $data = request_api("cart","del",array("id"=>  intval($_REQUEST['id'])));
            ajax_return($data);
        }
}
?>
