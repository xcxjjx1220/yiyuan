<?php
// +----------------------------------------------------------------------
// | fanwebbs.com 一元技术论坛
// +----------------------------------------------------------------------
// | Copyright (c) 2016 http://www.fanwebbs.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 微柚（5773389@qq.com）
// +----------------------------------------------------------------------

class cateModule extends MainBaseModule
{

	public function index()
	{
		global_run();
		init_app_page();

		$data_id = intval($_REQUEST['data_id']);
		
		$data = request_api("cate","index",array("data_id"=>$data_id));

		$GLOBALS['tmpl']->assign("data",$data);
		$GLOBALS['tmpl']->display("cate.html");
	}


}
?>
