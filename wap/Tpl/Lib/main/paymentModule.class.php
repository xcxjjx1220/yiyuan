<?php
// +----------------------------------------------------------------------
// | fanwebbs.com 一元技术论坛
// +----------------------------------------------------------------------
// | Copyright (c) 2016 http://www.fanwebbs.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 微柚（5773389@qq.com）
// +----------------------------------------------------------------------


class paymentModule extends MainBaseModule
{

	
	public function done()
	{
		global_run();
		init_app_page();
		$id = intval($_REQUEST['id']);

		$data = request_api("payment","done",array("id"=>$id));

		if(!$data['status'])
		{
			showErr($data['info']);
		}
		if($data['pay_status']==1)
		{			
			if($data['pay_type']==1){
				//会员充值夺宝币，跳到资金日志页面
				app_redirect(wap_url("index","uc_money"));
			}else{
				$data['page_title'] = "Payment result";
				$GLOBALS['tmpl']->assign("data",$data);
				$GLOBALS['tmpl']->display("payment_done.html");				
			}

		}
		else
		{
			$data['payment_code']['page_title'] = "Order payment";
			$GLOBALS['tmpl']->assign("data",$data['payment_code']);
			$GLOBALS['tmpl']->display("payment_pay.html");
		}
		
	}
	
	public function order_share(){
	    global_run();
	    init_app_page();
	    $id = intval($_REQUEST['id']);
	    $is_share = intval($_REQUEST['is_share']);

	    if($is_share){
	        $data = request_api("payment","order_share",array("id"=>$id));
	        if($data['user_login_status']!=LOGIN_STATUS_LOGINED){
	            app_redirect(wap_url("index","user#login"));
	            exit;
	        }
	    }
	    app_redirect(wap_url("index","uc_order#index",array('pay_status'=>1)));
	}
		
}
?>