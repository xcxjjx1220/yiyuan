<?php
// +----------------------------------------------------------------------
// | fanwebbs.com 一元技术论坛
// +----------------------------------------------------------------------
// | Copyright (c) 2016 http://www.fanwebbs.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 微柚（5773389@qq.com）
// +----------------------------------------------------------------------

class uc_chargeModule extends MainBaseModule
{

	public function index()
	{
		global_run();		
		init_app_page();

		
		$param=array();	
		$data = request_api("uc_charge","index",$param);
		
		if($data['user_login_status']!=LOGIN_STATUS_LOGINED){
			app_redirect(wap_url("index","user#login"));
		}
		
		$GLOBALS['tmpl']->assign("data",$data);	
		$GLOBALS['tmpl']->display("uc_charge.html");
	}
	
	public function done()
	{
		global_run();
		init_app_page();		
		$param=array();	
		$param['money'] = floatval($_REQUEST['money']);	
		$param['payment_id'] = intval($_REQUEST['payment_id']);	
		$data = request_api("uc_charge","done",$param);
		
		if($data['user_login_status']!=LOGIN_STATUS_LOGINED){
			app_redirect(wap_url("index","user#login"));
		}
		
		if(!$data['status'])
		{
			showErr($data['info']);
		}
		
		$GLOBALS['tmpl']->assign("data",$data);	
		$GLOBALS['tmpl']->display("uc_charge_done.html");	
	}


}
?>