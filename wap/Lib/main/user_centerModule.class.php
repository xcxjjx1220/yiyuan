<?php
// +----------------------------------------------------------------------
// | fanwebbs.com 一元技术论坛
// +----------------------------------------------------------------------
// | Copyright (c) 2016 http://www.fanwebbs.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 微柚（5773389@qq.com）
// +----------------------------------------------------------------------

class user_centerModule extends MainBaseModule
{

	public function index()
	{
		global_run();
		init_app_page();

		$param=array();
		$data = call_api_core("user_center","index",$param);
		if($data['user_login_status']!=LOGIN_STATUS_LOGINED){
			app_redirect(wap_url("index","user#login"));
		}

		$GLOBALS['tmpl']->assign("data",$data);
		$GLOBALS['tmpl']->display("user_center.html");
	}
	
	public function qrcode(){
	    global_run();
	    init_app_page();
	    $data['page_title'] ="Channel QR Code";
	    $data['user_id'] = intval($_REQUEST['data_id']);
	    
	    $user_id = $data['user_id'];
	    include_once APP_ROOT_PATH."system/model/weixin_jssdk.php";
	    $img_url = getQrCode($user_id);
	    
	    $GLOBALS['tmpl']->assign("img_url",$img_url);
	    $GLOBALS['tmpl']->assign("data",$data);
        $GLOBALS['tmpl']->display("qrcode.html");
	}
	
	 
	
	


}
?>
