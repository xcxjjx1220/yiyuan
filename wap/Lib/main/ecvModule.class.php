<?php
// +----------------------------------------------------------------------
// | fanwebbs.com 一元技术论坛
// +----------------------------------------------------------------------
// | Copyright (c) 2016 http://www.fanwebbs.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 微柚（5773389@qq.com）
// +----------------------------------------------------------------------

class ecvModule extends MainBaseModule
{

	public function index()
	{
		global_run();		
		init_app_page();		

		$param['exchange_sn'] = strim($_REQUEST['sn']); //分类ID

		$request = $param;
		//获取品牌
		$data = call_api_core("ecv","index",$param);

		if($data['user_login_status']!=LOGIN_STATUS_NOLOGIN){
		    $data['is_login'] = 1;
		}


		$GLOBALS['tmpl']->assign("data",$data);	
		$GLOBALS['tmpl']->assign("ajax_url",wap_url("index","ecv"));
		$GLOBALS['tmpl']->display("ecv.html");
	}
	

	
	public function do_snexchange(){
	   
	    global_run();

        /*获取参数*/
	    $exchange_sn = trim($_REQUEST['sn']);
	    $param=array();
	    $param['exchange_sn'] = $exchange_sn;
	    
	    $data = call_api_core("ecv","do_snexchange",$param);
		if ($data['user_login_status']!=LOGIN_STATUS_LOGINED){
			$data['status'] = -1;
			$data['info'] = "Please login first to receive";
	        $data['jump'] = wap_url("index","user#login");
	    }

	    ajax_return($data);
	    
	}
	
	
}
?>