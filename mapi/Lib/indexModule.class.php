<?php
// +----------------------------------------------------------------------
// | fanwebbs.com 一元技术论坛
// +----------------------------------------------------------------------
// | Copyright (c) 2016 http://www.fanwebbs.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 微柚（5773389@qq.com）
// +----------------------------------------------------------------------

class indexApiModule extends MainBaseApiModule
{
	

	public function wap()
	{
		global $is_app;
		$root = array();
		$root['return'] = 1;
		
		$city_id = $GLOBALS['city']['id'];
		$city_name =  $GLOBALS['city']['name'];
		
		$page = intval($GLOBALS['request']['page']);
		if($page==0){
			$page = 1;
		}
		
		$order = strim($GLOBALS['request']['order']);
		$order_dir=intval($GLOBALS['request']['order_dir']);
		
		if($order==''){
			$order='click_count';
		}
		$root['city_id'] = $city_id;
		$root['city_name'] = $city_name;
		$adv_list = $GLOBALS['cache']->get("WAP_INDEX_ADVS_".intval($city_id));
		
		//广告列表
		if($adv_list===false)
		{		
			if($is_app)
			{
				$sql = " select * from ".DB_PREFIX."m_adv where mobile_type = '0' and position=0 and city_id in (0,".intval($city_id).") and status = 1 order by sort desc ";
				$advs = $GLOBALS['db']->getAll($sql);
				if(empty($advs))
				{
					$sql = " select * from ".DB_PREFIX."m_adv where mobile_type = '1' and position=0 and city_id in (0,".intval($city_id).") and status = 1 order by sort desc ";
					$advs = $GLOBALS['db']->getAll($sql);
				}
			}			
			else
			{
				$sql = " select * from ".DB_PREFIX."m_adv where mobile_type = '1' and position=0 and city_id in (0,".intval($city_id).") and status = 1 order by sort desc ";
				$advs = $GLOBALS['db']->getAll($sql);
			}
				
				
			$adv_list = array();
			foreach($advs as $k=>$v)
			{
				$adv_list[$k]['id'] = $v['id'];
				$adv_list[$k]['name'] = $v['name'];
				$adv_list[$k]['img'] = get_abs_img_root($v['img']);  //首页广告图片规格为 宽: 640px 高: 240px
				$adv_list[$k]['type'] = $v['type'];
				$adv_list[$k]['data'] = $v['data'] = unserialize($v['data']);
				$adv_list[$k]['ctl'] = $v['ctl'];
			}
			$GLOBALS['cache']->set("WAP_INDEX_ADVS_".intval($city_id),$adv_list,300);
		}
		$root['advs'] = $adv_list?$adv_list:array();
		//$domain = app_conf("PUBLIC_DOMAIN_ROOT")==''?get_domain().APP_ROOT:app_conf("PUBLIC_DOMAIN_ROOT");
		//$root['get_domain'] = $domain;
		//return output($root);
		
		//首页菜单列表
		$indexs_list = $GLOBALS['cache']->get("WAP_INDEX_INDEX_".intval($city_id));
		if($indexs_list===false)
		{
			if($is_app)
			{
				$indexs = $GLOBALS['db']->getAll(" select * from ".DB_PREFIX."m_index where status = 1 and mobile_type = 0 and city_id in (0,".intval($city_id).") order by sort asc");
				if(empty($indexs))
					$indexs = $GLOBALS['db']->getAll(" select * from ".DB_PREFIX."m_index where status = 1 and mobile_type = 1 and city_id in (0,".intval($city_id).") order by sort asc");
			}
			else
			$indexs = $GLOBALS['db']->getAll(" select * from ".DB_PREFIX."m_index where status = 1 and mobile_type = 1 and city_id in (0,".intval($city_id).") order by sort asc");
			$indexs_list = array();
			foreach($indexs as $k=>$v)
			{
				$indexs_list[$k]['id'] = $v['id'];
				$indexs_list[$k]['name'] = $v['name'];
				$indexs_list[$k]['icon_name'] = $v['vice_name'];//图标名 http://fontawesome.io/icon/bars/
				$indexs_list[$k]['color'] = $v['desc'];//颜色
				$indexs_list[$k]['data'] = $v['data'] = unserialize($v['data']);
				$indexs_list[$k]['ctl'] = $v['ctl'];
			}
				
			$indexs_list=array_chunk($indexs_list,8);	
			$GLOBALS['cache']->set("WAP_INDEX_INDEX_".intval($city_id),$indexs_list,300);
		}
		
		$root['indexs'] = $indexs_list?$indexs_list:array();	
		
		

		require_once APP_ROOT_PATH."system/model/duobao.php";

		$index_duobao = duobao::get_list(0,$order,false,$page,$order_dir);
		$index_duobao_list=$index_duobao['list'];
		$total=$index_duobao['count'];
		foreach($index_duobao_list as $k=>$v)
		{
			$index_duobao_list[$k]['icon']=get_abs_img_root(get_spec_image($v['icon'],280,280,1));
			$index_duobao_list[$k]['progress']=round($v['current_buy']/$v['max_buy'],2)*100;
		}

		$root['index_duobao_list'] = $index_duobao_list?$index_duobao_list:array();
		
		
		$newest_doubao_list=duobao::get_newest_list(3);
		
		foreach($newest_doubao_list as $k=>$v)
		{
			$newest_doubao_list[$k]['icon']=get_abs_img_root(get_spec_image($v['icon'],280,280,1));

			
		}
		$root['newest_doubao_list']=$newest_doubao_list;
		
		
		$newest_lottery_list=duobao::get_lottery_list(10);
		
		foreach($newest_lottery_list as $k=>$v)
		{
			$newest_lottery_list[$k]['span_time']=duobao::format_lottery_time($v['lottery_time']);
		}
		
		//购物车
		$root['cart_info']=duobao::getcart($GLOBALS['user_info']['id']);
		
		//分页
		$page_size = PAGE_SIZE;
		$page_total = ceil($total/$page_size);
		$root['order']=$order;
		$root['order_dir']=$order_dir;
		
		$root['newest_lottery_list']=$newest_lottery_list;
		$root['page'] = array("page"=>$page,"page_total"=>$page_total,"page_size"=>$page_size,"data_total"=>$total);
		//推荐位
		$root['zt_html'] = load_zt();
		$root['now_time']=NOW_TIME;
		$root['page_title'] = $GLOBALS['m_config']['program_title']?$GLOBALS['m_config']['program_title']." - ":"";
		$root['page_title'].="Home";
		$root['mobile_btns_download'] = wap_url("index","app_download");
		
		//分享url
		$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $root['share_url'] = $url.'&r='.base64_encode($GLOBALS['user_info']['id']);
		return output($root);
	}
        
        /**
         * 异步加载首页数据
         */
        public function wap_load_page(){
            $page = intval($GLOBALS['request']['page']);
            $order = strim($GLOBALS['request']['order']);
            $order_dir=intval($GLOBALS['request']['order_dir']);
            
            if($order==''){
                $order='click_count';
            }
                
            require_once APP_ROOT_PATH."system/model/duobao.php";

            $index_duobao = duobao::get_list(0,$order,false,$page,$order_dir);
            $index_duobao_list=$index_duobao['list'];
            $total=$index_duobao['count'];
            foreach($index_duobao_list as $k=>$v)
            {
                    $index_duobao_list[$k]['icon']=get_abs_img_root(get_spec_image($v['icon'],280,280,1));
                    $index_duobao_list[$k]['progress']=round($v['current_buy']/$v['max_buy'],2)*100;
            }

            $root['index_duobao_list'] = $index_duobao_list?$index_duobao_list:array();
            //分页
            $page_size = PAGE_SIZE;
            $page_total = ceil($total/$page_size);
            $root['page'] = array("page"=>$page,"page_total"=>$page_total,"page_size"=>$page_size,"data_total"=>$total);
            $root['order']=$order;
            $root['order_dir']=$order_dir;

            return output($root);
        }

	
}
?>