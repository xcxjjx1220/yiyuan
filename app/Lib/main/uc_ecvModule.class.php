<?php
// +----------------------------------------------------------------------
// | Fanwe 一元o2o商业系统
// +----------------------------------------------------------------------
// | Copyright (c) 2016 http://www.fanwebbs.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 微柚（5773389@qq.com）
// +----------------------------------------------------------------------

class uc_ecvModule extends MainBaseModule
{
	public function index()
	{		
		global_run();
		init_app_page();
		$data['page_title'] = 'My red envelope';

		$GLOBALS['tmpl']->display("uc_ecv_index.html");
	}
	
	public function exchange(){
	    global_run();
	    init_app_page();
	    $data['page_title'] = 'Red envelope exchange';
	    
	    $GLOBALS['tmpl']->display("uc_ecv_exchange.html");
	}
	
	
}
?>